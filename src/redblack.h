/*
 * redblack.h
 *
 *  Created on: 7 Apr 2020
 *      Author: Derek
 */

#include <stdlib.h>
//#include "fatal.h"

typedef int ElementType;
#define NegInfinity (-10000)

#ifndef REDBLACK_H_
#define REDBLACK_H_

struct RedBlackNode;
typedef struct RedBlackNode *Position;
typedef struct RedBlackNode *RedBlackTree;

RedBlackTree MakeEmpty(RedBlackTree T);
Position Find(ElementType X, RedBlackTree T);
Position FindMin(RedBlackTree T);
Position FindMax(RedBlackTree T);
RedBlackTree Initialize(void);
RedBlackTree Insert(ElementType X, RedBlackTree T);
RedBlackTree Remove(ElementType X, RedBlackTree T);
ElementType Retrieve(Position P);
void PrintTree(RedBlackTree T);

#endif /* REDBLACK_H_ */

