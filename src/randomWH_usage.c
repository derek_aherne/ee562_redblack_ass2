/*!
 * @file randomWH_usage.h
 * @author Martin Collier
 * @date 11 February 2014
 * @brief A test program to evaluate the pseudorandom number generator
 *
 * @see randomWH.h
 *
 * @version 0.2 Alpha
 *
 *
 * <B> @em Usage </B><P>
 * Assuming that all your source files are in the one directory, and you are using the Gnu compiler, type
 * (in a Linux terminal) "gcc -o test randomWH_usage.c randomWH.c -lm", followed by "./test". In a Windows OS,
 * it may be simpler to let your IDE figure out the command line.
 *
 * The latest version can also be built using the command " g++ -std=c++11 -o ccc.exe randomWH_usage.c randomWH.c".
 * This means that you can use C++ syntax in randomWH_usage.c or replace it in the command line with (say) assignment.cpp
 *
 *
 * @copyright
 *   &copy; 2014 Martin Collier.
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *   http://www.apache.org/licenses/LICENSE-2.0.
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include "redblack.h"
#include "randomWH.h"
#define SIM_DURATION 40000000L
#define N 800
/* Example of struct initialisation
 * A struct called congruent is pre-initialised for you
 */

void Congruence_dump(Congruence* generatorZ);

void Congruence_dump(Congruence* generatorZ){
	long int* read_con;
	int count;
	char seednames[]={'x','y','z','t'};
	read_con=generatorZ->read(generatorZ);
	for(count=0;count<4;count++)
		printf("seed %c = %ldL\n", seednames[count], read_con[count]);
}


int main(){
	/*********************
	 * RANDOM NUMBER MAIN
	 *
	 *********************/
	long int count;
	int i;
	long int my_num;

/* Generate a sequence and calculate its first and second moment */
	congruent.init(&congruent,1L,2L,3L,4L);

	struct node_entry{
	        long int finish_key;
	        long int vpn;
	};

	struct node_entry node_entry[100] = {};

	for(count =1;count<=100;count++){
			my_num = congruent.fairdie(&congruent,0,999L);
			node_entry[count] = (struct node_entry){count, my_num};
		}

	// print
	for (i = 1; i <= sizeof(node_entry) / sizeof(node_entry[0]); i++)
	    printf("%ld: %ld\n",  node_entry[i].finish_key, node_entry[i].vpn);

	/*********************
	 * RED BLACK MAIN
	 *
	*********************/

    RedBlackTree T;
    Position P;

    int j = 0;

    T = Initialize();
    T = MakeEmpty(T);

    for (i = 0; i < N; i++, j = (j + 7) % N)
        T = Insert(j, T);
    	printf("Inserts are complete\n");

    for (i = 0; i < N; i++)
        if ((P = Find(i, T)) == NULL || Retrieve(P) != i)
            printf("Error at %d\n", i);

    printf("Min is %d, Max is %d\n", Retrieve(FindMin(T)),
            Retrieve(FindMax(T)));


}
